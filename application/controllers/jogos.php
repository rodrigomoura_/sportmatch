<?php
Class Jogos extends CI_Controller{    
    public function  __construct(){
        parent::__construct();
        $this->load->model('Jogo_model');
        $this->load->model('Usuario');
        $this->load->model('cep');
        $this->load->model('Amigos_model');
        $this->load->library('googlemaps');
    }

    public function index(){
    	$id_user = $this->session->userdata('id');
    	$data['jogos'] = $this->Jogo_model->busca_jogos2($id_user);
        $data['usuario'] = $this->Usuario->get_usuarios();
        //print_r($data);

    	$this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu');
        $this->load->view('jogos',$data);
        $this->load->view('html_footer');
    }

    function maisDetalhes($idJogo){
        $id_user = $this->session->userdata['id'];
        $data['jogo'] = $this->Jogo_model->get_jogo($idJogo);
        $data['jogoUsuario'] = $this->Jogo_model->get_jogo_usuario($idJogo);
        $data['confirmado'] = $this->Jogo_model->get_usuario_confirmado($id_user, $idJogo);
        //print_r($data);
        $this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu');
        $this->load->view('detalhesJogo', $data);
        $this->load->view('html_footer');
    }

    function novo_jogo(){ //view para cadastro
        $id = $this->session->userdata('id');
        $data['esportes'] = $this->Jogo_model->pega_jogo();
        $estado = $this->cep->pega_estado($id);
        $uf= $estado[0]->estado;
        $data['cidades'] = $this->cep->pega_cidade($uf);

        $this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu');
        $this->load->view('criaJogo', $data);
        $this->load->view('html_footer');
    }

    function novoJogo(){ //insere no banco
        $id = $this->session->userdata('id');
        $estado = $this->cep->pega_estado($id);
        $uf = $estado[0]->estado;

        $data['tipo_jogo'] = $this->input->post('tipoEsporte');
        $data['horario_inicio'] = $this->input->post('horario_inicio');
        $data['horario_fim'] = $this->input->post('horario_fim');
        $data['participantes'] = $this->input->post('participantes');
        $data['data'] = $this->input->post('data');
        $data['estado'] = $uf;
        $data['cidade'] = $this->input->post('cidade');
        $data['bairro'] = $this->input->post('bairro');
        $data['endereco'] = $this->input->post('endereco');
        $data['id_criador'] = $this->session->userdata('id');

        //print_r($data);
        $this->Jogo_model->novo_jogo($data);
        $amigos = $this->Amigos_model->get_lista_amigos($id);
        $idJogo = $this->Jogo_model->pegaJogo($id);

        for($i=0;$i<count($idJogo);$i++){
            if($i==count($idJogo)-1){
                $idJoga = $idJogo[$i]->id_jogo;
            }
        }

        //echo $idJoga;
        //print_r($idJogo);
        $dados2['id_usuario'] = $id;
        $dados2['id_jogo'] = $idJoga;
        $dados2['tipo'] = 'confirmado';
        $this->Jogo_model->insereJogoCriador($dados2);
        //print_r($amigos);
        for($i=0;$i<count($amigos);$i++){
            $idAmigo = $amigos[$i]->id_usuario1;
            $dados['id_usuario'] = $idAmigo;
            $dados['id_jogo'] = $idJoga;
            $dados['tipo'] = 'convite';
            $this->Jogo_model->convida_amigos($dados);
        }
        redirect('jogos/index');
    }

    function excluiJogo($idJogo){
        //$this->Jogo_model->exclui_jogo_usuario($idJogo);
        $this->Jogo_model->exclui_jogo($idJogo);
        redirect('jogos/index');
    }

    function editaJogo($idJogo){
        $data['jogo'] = $this->Jogo_model->get_jogo($idJogo);
        $id = $this->session->userdata('id');
        $data['esportes'] = $this->Jogo_model->pega_jogo();
        $estado = $this->cep->pega_estado($id);
        $uf= $estado[0]->estado;
        $data['cidades'] = $this->cep->pega_cidade($uf);

        $this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu');
        $this->load->view('editaJogo', $data);
        $this->load->view('html_footer');
    }

    function finalizaEdicao(){
        $id = $this->session->userdata('id');
        $estado = $this->cep->pega_estado($id);
        $uf = $estado[0]->estado;

        $data['tipo_jogo'] = $this->input->post('tipoEsporte');
        $data['horario_inicio'] = $this->input->post('horario_inicio');
        $data['horario_fim'] = $this->input->post('horario_fim');
        $data['participantes'] = $this->input->post('participantes');
        $data['data'] = $this->input->post('data');
        $data['estado'] = $uf;
        $data['cidade'] = $this->input->post('cidade');
        $data['bairro'] = $this->input->post('bairro');
        $data['endereco'] = $this->input->post('endereco');
        $data['id_criador'] = $this->session->userdata('id');

        $this->Jogo_model->edita_jogo($data, $id);
        redirect('jogos/index');
    }

    function participar($idJogo){
        $idUser = $this->session->userdata('id');
        $data['id_usuario'] = $this->session->userdata('id');
        $data['id_jogo'] = $idJogo;
        $data['tipo'] = 'confirmado';
        $this->Jogo_model->confirma_participacao($data, $idUser, $idJogo);

        redirect('jogos/index');
    }

    function enviaNota($idJogo){
        $data['jogo_usuario'] = $this->Jogo_model->get_jogo_usuario($idJogo);
        $data['idJogo'] = $idJogo;

        $this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu');
        $this->load->view('envia_nota',$data);
        $this->load->view('html_footer');   
    }

    function pegaNotas($idJogo){
        $idUser = $this->session->userdata('id');
        $jogo_usuario = $this->Jogo_model->get_jogo_usuario($idJogo);
        for($i=0;$i<count($jogo_usuario);$i++){
            $data['id_usuario_jogo'] = $jogo_usuario[$i]->id;
            $data['nota'] = $this->input->post($jogo_usuario[$i]->id_facebook);
            $this->Jogo_model->nota($data, $idUser);
            $idReal = $this->Usuario->pegaIDreal($idUser);
            $idUsuario = $idReal[0]->id;
            $nNotas = $this->Jogo_model->pegaNnotas($idUsuario);
            $mediaNota = 0;
            for($k=0;$k<count($nNotas);$k++){
                $mediaNota = $mediaNota + $nNotas[$k]->nota;
            }
            $mediaNota = $mediaNota/(count($nNotas));
            $this->Jogo_model->update_media($idUser, $mediaNota);
        }
        redirect('jogos/index');
        //print_r($data);
    }

    function verMapa($idJogo){
        $idUser = $this->session->userdata('id');
        $enderecoUser = $this->cep->endereco_user($idUser);
        $enderecoJogo = $this->cep->endereco_jogo($idJogo);

        //mapa
        /*$config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $config['directions'] = TRUE;
        //$config['directionsStart'] = $enderecoUser[0]->endereco." ".$enderecoUser[0]->cidade;
        //$config['directionsEnd'] = $enderecoJogo[0]->endereco." ".$enderecoJogo[0]->cidade;
        $config['directionsStart'] = 'Brasil';
        $config['directionsEnd'] = 'Estados Unidos';
        $config['directionsDivID'] = 'directionsDiv';*/
        $config['center'] = 'auto';
        $config['zoom'] = 'auto';
        $config['directions'] = TRUE;
        $config['directionsStart'] = $enderecoUser[0]->rua." ".$enderecoUser[0]->cidade;
        $config['directionsEnd'] = $enderecoJogo[0]->endereco." ".$enderecoJogo[0]->cidade;
        $config['directionsDivID'] = 'directionsDiv';
        $this->googlemaps->initialize($config);     
        $data['map'] = $this->googlemaps->create_map();
        


        $this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu');
        $this->load->view('mapas',$data);
        $this->load->view('html_footer'); 
    }
}