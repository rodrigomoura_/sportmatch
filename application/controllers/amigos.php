<?php
Class Amigos extends CI_Controller{    
    public function  __construct(){
        parent::__construct();
        $this->load->model('Amigos_model');
    }

    function index(){
        $id_user = $this->session->userdata('id');
        $data['amizade'] = $this->Amigos_model->get_amigos_amizade($id_user);

        $this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu');
        $this->load->view('amizades',$data);
        $this->load->view('html_footer');   
    }

    function busca(){
        $nome = $this->input->post('nome');
        //$data['busca'] = $this->db->query('SELECT * FROM usuario WHERE nome LIKE "%'.$nome.'%"')->result();
        $data['busca'] = $this->Amigos_model->like_amigos($nome);
        $id_amigo = $this->Amigos_model->pegaIDAmigo($nome);
        //$idAmigo = $data['busca'];
        //$data['amizade'] = $this->db->get('amizade')->result();
        $id_amizade = $id_amigo[0]->id_facebook;
        $id_user = $this->session->userdata('id');
        $data['amizade'] = $this->Amigos_model->pega_amizade($id_amizade,$id_user);

        $this->load->view('html_header');
        $this->load->view('navbar');
        $this->load->view('menu'); 
        $this->load->view('buscaAmigos',$data);
        $this->load->view('html_footer');
    }

    function get_amigos(){
        if (isset($_GET['term'])){
            $busca = strtolower($_GET['term']);
            $this->Amigos_model->get_amigo($busca);
        } 
    }

    function nova_amizade($idNovoAmigo){
        $id_user = $this->session->userdata('id');
        $this->Amigos_model->nova_amizade($id_user,$idNovoAmigo);
        redirect('amigos/index');
    }

    function exclui_amigo($id){
        $this->Amigos_model->exclui_amigo($id);
        redirect('amigos/index');
    }

}