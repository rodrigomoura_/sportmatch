<?php
Class Inicio extends CI_Controller{    
    public function  __construct(){
        parent::__construct();
        if(!$this->session->userdata('session_id') || !$this->session->userdata('logado')){
            redirect("welcome/index");
        }
        $this->load->model('Usuario'); //carrega o model Usuario
        $this->load->model('Jogo_model');
        $this->load->model('cep');
    }

    function index(){
        $id = $this->session->userdata('id');
        $data['jogos'] = $this->Jogo_model->pega_jogo();
        //$data['cidades'] = $this->cep->pega_cidade($id);
        $estado = $this->cep->pega_estado($id);
        $uf= $estado[0]->estado;
        $data['cidades'] = $this->cep->pega_cidade($uf);

        $this->load->view('html_header'); //sempre será carregada junto com as outras views
        $this->load->view('navbar'); //view do navbar, que é a barra superior com informações do usuario, logo do sportmatch, etc
        $this->load->view('menu'); //view do menu             
        $this->load->view('conteudo',$data); //view que será carregada com o conteudo, busca de horarios/jogos, jogos confirmados,etc
        $this->load->view('html_footer'); //sempre será carregada junto com as outras views
    }
 }