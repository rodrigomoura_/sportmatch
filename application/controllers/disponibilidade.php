<?php
Class Disponibilidade extends CI_Controller{    
    public function  __construct(){
        parent::__construct();
        $this->load->model('Jogo_model');
        $this->load->model('Usuario');
        $this->load->model('cep');
        $this->load->model('Amigos_model');
        $this->load->model('disponibilidade_model');
    }

    function cadastra(){
    	$idUser = $this->session->userdata('id');
    	$data['id_usuario'] = $this->session->userdata('id');
    	$data['tipo_esporte'] = $this->input->post('tipo_esporte');
    	$data['data'] = $this->input->post('data_jogo');
    	$data['horario_inicio'] = $this->input->post('h_ini');
    	$data['horario_fim'] = $this->input->post('h_fim');
    	$data['cidade'] = $this->input->post('cidade');
    	$tipo_esporte = $this->input->post('tipo_esporte');
    	$data_jogo = $this->input->post('data_jogo');
    	$cidade = $this->input->post('cidade');

    	$dados['jogos'] = $this->disponibilidade_model->procuraJogo($tipo_esporte, $data_jogo, $cidade);
    	//print_r($dados);

    	if(count($dados)==0){
    		$this->disponibilidade_model->cadastra_disponibilidade($data);
    		redirect('inicio/index');
    	}else{
    		$dados['amigos'] = $this->Amigos_model->get_lista_amigos($idUser);
    		$dados['usuarios'] = $this->Usuario->get_usuarios();
    		//print_r($dados);
    		$this->load->view('html_header');
        	$this->load->view('navbar');
       		$this->load->view('menu');
        	$this->load->view('resultadoBusca',$dados);
        	$this->load->view('html_footer');
    	}
    }

    function criaJogo($idDisponibilidade){
    	$disponibilidade = $this->disponibilidade_model->pega_disponibilidade($idDisponibilidade);
    	$idUser = $this->session->userdata('id');

    	$data['tipo_jogo'] = $disponibilidade[0]->tipo_esporte;
        $data['horario_inicio'] = '';
        $data['horario_fim'] = '';
        $data['participantes'] = '';
        $data['data'] = $disponibilidade[0]->data;
        $data['estado'] = '';
        $data['cidade'] = '';
        $data['bairro'] = '';
        $data['endereco'] = '';
        $data['id_criador'] = $this->session->userdata('id');

        $this->Jogo_model->novo_jogo($data);
        $amigos = $this->Amigos_model->get_lista_amigos($idUser);
        $idJogo = $this->Jogo_model->pegaJogo($idUser);

        for($i=0;$i<count($idJogo);$i++){
            if($i==count($idJogo)-1){
                $idJoga = $idJogo[$i]->id_jogo;
            }
        }

        //echo $idJoga;
        //print_r($idJogo);
        $dados2['id_usuario'] = $idUser;
        $dados2['id_jogo'] = $idJoga;
        $dados2['tipo'] = 'confirmado';
        $this->Jogo_model->insereJogoCriador($dados2);
        //print_r($amigos);
        for($i=0;$i<count($amigos);$i++){
            $idAmigo = $amigos[$i]->id_usuario1;
            $dados['id_usuario'] = $idAmigo;
            $dados['id_jogo'] = $idJoga;
            $dados['tipo'] = 'convite';
            $this->Jogo_model->convida_amigos($dados);
        }
        redirect('jogos/index');

    }

}