<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Adm extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		//$this->load->view('adm/menu');
		$this->load->view('adm/login');
		$this->load->view('adm/html_footer');
	}

	public function login(){
		$this->load->library('encrypt');
		$usuario = $this->input->post('login');
		$senha = $this->input->post('senha');

		$this->db->where('login',$usuario);
		$login = $this->db->get('admin')->result();
		$crip = $login[0]->senha;

		$decrip = $this->encrypt->decode($crip);

		if($decrip==$senha){
			$dados = array('login' => $login[0]->login, 'logado' => TRUE);
			$this->session->set_userdata($dados);
			redirect("adm/gerencia/");			
		}else{
			redirect("adm/home/index");
		}

		$this->db->where('login',$usuario);
		$this->db->where('senha',$senha);
		$usuario = $this->db->get('admin')->result();


		if(count($usuario)==1){
			$dados = array('usuario' => $usuario[0]->usuario, 'logado' => TRUE);
			$this->session->set_userdata($dados);
			redirect("adm/gerencia");
		}else{
			redirect("adm/index");
		}
	}


	/*public function grava(){
		$login = $this->input->post('login');
		$senha = $this->input->post('senha');
		$this->load->library('encrypt');
		$cript = $this->encrypt->encode($senha);
		$data['login'] = $this->input->post('login');
		$data['senha'] = $cript;
		$this->db->insert('admin',$data);
	}*/

}