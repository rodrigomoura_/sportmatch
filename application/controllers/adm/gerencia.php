<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Gerencia extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('session_id') || 
			!$this->session->userdata('logado')){
			redirect("adm/adm");
		}
	}

	public function index(){
		$this->load->view('adm/html_header');
		$this->load->view('adm/navbar');
		$this->load->view('adm/menu');
		$this->load->view('adm/html_footer');
	}

	public function usuarios(){
		$data['usuarios'] = $this->db->get('usuario')->result();
		$this->load->view('adm/html_header');
		$this->load->view('adm/navbar');
		$this->load->view('adm/menu');
		$this->load->view('adm/usuarios',$data);
		$this->load->view('adm/html_footer');
	}
	public function jogos(){
		$data['jogo'] = $this->db->get('jogo')->result();
		$this->load->view('adm/html_header');
		$this->load->view('adm/navbar');
		$this->load->view('adm/menu');
		$this->load->view('adm/jogos',$data);
		$this->load->view('adm/html_footer');
	}

	function excluiUsuario1($idUser){
		$this->db->where('id_facebook', $idUser);
		$data['usuario'] = $this->db->get('usuario')->result();
		$this->load->view('adm/html_header');
		$this->load->view('adm/navbar');
		$this->load->view('adm/menu');
		$this->load->view('adm/excluiUsuario',$data);
		$this->load->view('adm/html_footer');
	}

	function excluiUsuario2($idUser){
		$this->db->where('id_facebook', $idUser);
		$usuario = $this->db->get('usuario')->result();

		$dados['nome'] = $usuario[0]->nome;
		$dados['email'] = $usuario[0]->email;
		$dados['mensagem'] = $this->input->post('email');


		$this->db->where('id_facebook', $idUser);
		$this->db->delete('usuario');
		$this->db->where('id_usuario', $idUser);
		$this->db->delete('jogo_usuario');

		$this->load->library('email');
		$this->email->from($usuario[0]->email, $usuario[0]->nome);
		$this->email->to($usuario[0]->email);
		$this->email->message($this->input->post('email'));
		$this->email->send();

		redirect('adm/gerencia');
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect("adm/adm/index");
	}


	/*public function index(){
		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdm');
		$this->load->view('adm/html_footer');
	}
	//fim index - inicio carousel

	public function carousel(){
		$data['carousel'] = $this->db->get('carousel')->result();

		$this->load->library('image_lib'); 

		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdmCarousel');
		$this->load->view('adm/carousel', $data);
		$this->load->view('adm/html_footer');
	}

	function excluirCarousel($id){
		$this->db->where('id',$id);
		$this->db->delete('carousel');
		redirect("adm/gerencia/carousel");
	}

	function addCarousel(){
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}	
		else{
			$data = array('upload_data' => $this->upload->data());
			$carousel['nome_foto'] = $data['upload_data']['file_name'];
			$carousel['texto'] = $this->input->post('texto');
			$carousel['id_produto'] = $this->input->post('codigo');
			$this->db->insert('carousel', $carousel);
			redirect('adm/gerencia/carousel');
		}
	}

	//fim carousel - inicio freteGratis

	public function freteGratis(){
		$data['cidades'] = $this->db->get_where('tb_cidades',array('uf'=>'RS'))->result();
		$data['freteGratis'] = $this->db->get('fretegratis')->result();

		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdmFrete');
		$this->load->view('adm/freteGratis', $data);
		$this->load->view('adm/html_footer');
	}

	function addFrete(){
		$data['cidade'] = $this->input->post('cidade');
		$this->db->insert('fretegratis',$data);
		redirect('adm/gerencia/freteGratis');
	}

	function excluirFrete($id){
		$this->db->where('id',$id);
		$this->db->delete('fretegratis');
		redirect("adm/gerencia/freteGratis");
	}
	//fim frete
	//inicio produtos
	public function produtos(){
		$data['categoria'] = $this->db->get('categoria')->result();
		$data['subcategoria'] = $this->db->query("SELECT DISTINCT nome FROM `subcategoria` ORDER BY nome")->result();

		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdmProdutos');
		$this->load->view('adm/buscaProdutos',$data);
		$this->load->view('adm/addProduto', $data);
		$this->load->view('adm/html_footer');
	}

	function buscaProdutos(){
		if(($busca = $this->input->post('busca1')) == "Todos"){
			$busca = $this->input->post('busca2');
			$data2['busca1'] = $this->db->query('SELECT * FROM menphis WHERE descricao LIKE "%'.$busca.'%"')->result();
		}else{
			$busca1 = $this->input->post('busca1');
			$data2['busca1'] = $this->db->query('SELECT * FROM menphis WHERE descricao LIKE "%'.$busca1.'%"')->result();
		}
		//$busca = "camisa";
		//$data2['busca1'] = $this->db->query('SELECT * FROM menphis WHERE descricao LIKE "%'.$busca.'%"')->result();
		//-----------------------------------------------------------------
		$data['subcategoria'] = $this->db->query("SELECT DISTINCT nome FROM `subcategoria` ORDER BY nome")->result();

		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdmProdutos');
		$this->load->view('adm/buscaProdutos',$data);
		$this->load->view('adm/mostraBusca', $data2);
		$this->load->view('adm/html_footer');
	}

	function addProduto(){
		$data['codigo'] = $this->input->post('codigo');
		$data['descricao'] = $this->input->post('descricao');
		$data['marca'] = $this->input->post('marca');
		$data['unidade'] = $this->input->post('unidade');
		$data['quantidade'] = $this->input->post('quantidade');
		$data['tamanho'] = $this->input->post('tamanho');
		$data['venda'] = $this->input->post('venda');
		$data['categoria'] = $this->input->post('categoria');
		$data['subcategoria'] = $this->input->post('subcategoria');

		$this->db->insert('menphis',$data);
		redirect('adm/gerencia/produtos');
	}

	function editarProduto($codigo){
		$this->db->where('codigo',$codigo);
		$data['produto'] = $this->db->get('menphis')->result();
		$data['categoria'] = $this->db->get('categoria')->result();
		$data['subcategoria'] = $this->db->query("SELECT DISTINCT nome FROM `subcategoria` ORDER BY nome")->result();

		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdmProdutos');
		$this->load->view('adm/editaProduto',$data);
		$this->load->view('adm/html_footer');		
	}
	//salva as alterações no bd
	function editaProduto(){
		$cod = $this->input->post('codigo2');
		$data['codigo'] = $this->input->post('codigo2');
		$data['descricao'] = $this->input->post('descricao2');
		$data['marca'] = $this->input->post('marca2');
		$data['unidade'] = $this->input->post('unidade2');
		$data['quantidade'] = $this->input->post('quantidade2');
		$data['tamanho'] = $this->input->post('tamanho2');
		$data['venda'] = $this->input->post('venda2');
		$data['categoria'] = $this->input->post('categoria');
		$data['subcategoria'] = $this->input->post('subcategoria');

		$this->db->where('codigo',$cod);
		$this->db->update('menphis',$data);
		redirect('adm/gerencia/produtos');
	}

	function excluirProduto($codigo){
		$this->db->where('codigo',$codigo);
		$this->db->delete('menphis');
		redirect("adm/gerencia/produtos");
	}
	//fiiiim produtos

	public function valePresente(){
		$data['vale'] = $this->db->get('vale_presente')->result();

		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdmVale');
		$this->load->view('adm/valePresente',$data);
		$this->load->view('adm/html_footer');
	}

	function addVale(){
		$data['numeroVale'] = $this->input->post('nVale');
		$data['valor_vale'] = $this->input->post('vVale');
		$this->db->insert('vale_presente',$data);
		redirect('adm/gerencia/valePresente');
	}

	function excluirVale($id){
		$this->db->where('id',$id);
		$this->db->delete('vale_presente');
		redirect("adm/gerencia/valePresente");
	}
	//fim vale presente

	public function gerenciarFotos(){
		$this->load->view('adm/html_header');
		$this->load->view('adm/navbarAdm');
		$this->load->view('adm/menuAdmFotos');
		$this->load->view('adm/fotos');
		$this->load->view('adm/html_footer');
	}

	function addFotoP(){
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}	
		else{
			$data = array('upload_data' => $this->upload->data());
			$foto['nome_foto'] = $data['upload_data']['file_name'];
			$foto['id_produto'] = $this->input->post('codigo');
			$this->db->insert('foto_principal', $foto);
			redirect('adm/gerencia/gerenciarFotos');
		}
	}

	function addFotoS(){
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}	
		else{
			$data = array('upload_data' => $this->upload->data());
			$foto['nome_foto'] = $data['upload_data']['file_name'];
			$foto['id_produto'] = $this->input->post('codigo');
			$this->db->insert('foto_secundaria', $foto);
			redirect('adm/gerencia/gerenciarFotos');
		}
	}



	//logout
	public function logout(){
		$this->session->sess_destroy();
		redirect("adm/gerencia/index");
	}*/
}
