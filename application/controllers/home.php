<?php
Class Home extends CI_Controller{    
    public function  __construct(){
        parent::__construct();

        if(!$this->session->userdata('session_id')){
            redirect("welcome/index");
        }

        $this->load->model('Usuario'); //carrega o model Usuario
        $this->load->model('cep');
    }

    function verificaCadastro($id){
        $dados['cadastro'] = $this->Usuario->verificaID($id); //chama o model Usuario, o método verificaID, passando o ID do facebook do usuario que logou-se
        $this->db->where('id_face',$id);
        $data['user'] = $this->db->get('dadosprovisorios')->result();
        if($dados['cadastro']){ // verifica se o usuario já está cadastrado, se estiver, vai para a página inicial
            $nome = $this->Usuario->pegaNome($id);
            $this->Usuario->excluiDadoProvisorio_model($id);
            $dados = array('usuario' => $nome[0]->name, 'logado' => TRUE, 'id' => $id);
            $this->session->set_userdata($dados);
            redirect('inicio');
        }else{ //se não estiver, vai a página de cadastro
            $data['estados'] = $this->cep->estados();
            $this->load->view('html_header');
            $this->load->view('navbarCadastro'); 
            $this->load->view('formCadastro',$data); //view com formulário do cadastro
            $this->load->view('html_footer');
        }
    }

    function pega_cidades(){
        $options = "";
        if($this->input->post('estado')){
            $estado = $this->input->post('estado');
            $cidades = $this->cep->cidade($estado);
            foreach($cidades as $cid){
            ?>
                <option value="<?php echo $cid->nome ?>"><?php echo $cid->nome; ?></option> 
            <?php }//muuuuudei aquuiiii
        }
    }

    function addUsuario(){ //método que recebe do formulário os dados do novo usuario, e manda para o model que adiciona ao BD
        $dados['nome'] = $this->input->post('nome');
        $dados['email'] = $this->input->post('email');
        $dados['cpf'] = $this->input->post('cpf');
        $dados['data_nascimento'] = $this->input->post('dtnasc');
        $dados['sexo'] = $this->input->post('sexo');
        $dados['rua'] = $this->input->post('rua');
        $dados['bairro'] = $this->input->post('bairro');
        $dados['cidade'] = $this->input->post('cidade');
        $dados['estado'] = $this->input->post('estado');
        $dados['id_facebook'] = $this->input->post('id_face');
        $dados['bairro'] = $this->input->post('bairro');
        $dados['url_foto'] = 'bootstrap/img/semPerfil.jpg';

        $id = $this->input->post('id_face');
        //$nome = $this->Usuario->pegaNome($id);
        $nome = $this->input->post('nome');
        $this->Usuario->excluiDadoProvisorio_model($id);
        $this->Usuario->addUsuario_model($dados);

        $dados = array('usuario' => $nome, 'logado' => TRUE, 'id' => $id);

        $this->session->set_userdata($dados);
        redirect('inicio/index');
    }

    function excluiUsuario($id){
        $this->Usuario->excluiUsuario_model($id);
    }

    function conta(){
        $idUser = $this->session->userdata('id');
        $dados['usuario'] = $this->Usuario->pega_usuario($idUser);
        $dados['estados'] = $this->cep->estados();

        $this->load->view('html_header');
        $this->load->view('navbar'); 
        $this->load->view('conta_usuario',$dados);
        $this->load->view('html_footer');
    }

    function editaUsuario(){
        $dados['nome'] = $this->input->post('nome');
        $dados['email'] = $this->input->post('email');
        $dados['cpf'] = $this->input->post('cpf');
        $dados['data_nascimento'] = $this->input->post('dtnasc');
        $dados['sexo'] = $this->input->post('sexo');
        $dados['rua'] = $this->input->post('rua');
        $dados['bairro'] = $this->input->post('bairro');
        $dados['cidade'] = $this->input->post('cidade');
        $dados['estado'] = $this->input->post('estado');
        $dados['id_facebook'] = $this->input->post('id_face');
        $dados['bairro'] = $this->input->post('bairro');
        $dados['url_foto'] = 'bootstrap/img/semPerfil.jpg';

        $idUser = $this->input->post('id_face');

        $this->Usuario->editaUsuario_model($dados, $idUser);
        redirect('inicio/index');
    }
}
