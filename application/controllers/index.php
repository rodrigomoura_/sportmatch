<?php
Class Index extends CI_Controller
{
    
    public function  __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }
    
    public function index()
    {   
        
        $this->data['titulo'] = 'SportMatch';
        $this->fb();
        $this->load->view('html_header', $this->data);
        $this->load->view('index', $this->data);
        $this->load->view('html_footer');
    }

    private function fb()
    {
	$config = array
        (
            'appId'  => '1488917458037056',
            'secret' => '784dbf7e9b5f0423f118836d7c6573a8'
    	);
	$this->load->library('facebook', $config);
	$user = $this->facebook->getUser();
	$loginParams = array('scope' => 'email','redirect_uri' => site_url().'index/home/'); //permissoes, criar array onde tem o email
	$this->data['login_url'] = $this->facebook->getLoginUrl($loginParams);
	$logoutParams = array( 'next' => site_url().'http://localhost/php/SportMatch/');
	$this->data['logout_url'] = $this->facebook->getLogoutUrl($logoutParams);
    }
	
    public function fb_auth()
    {
	$config = array
        (
            'appId'  => '21488917458037056',
            'secret' => '784dbf7e9b5f0423f118836d7c6573a8'
        );
	$this->load->library('facebook', $config);
	$user = $this->facebook->getUser();
	if($user)
        {
            try 
            {
              $user_profile = $this->facebook->api('/me');
    		  $this->session->set_userdata('user_profile', $user_profile);
    		  redirect(site_url());
            }
            catch (FacebookApiException $e)
            {
              $user = null;
            }
        }
    }
    
    public function logout(){
        $this->session->unset_userdata('user_profile');
        redirect(site_url());
    }
}
	