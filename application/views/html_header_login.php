<?php echo doctype('xhtml1-trans'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SportMatch</title>
		<?php
		$meta = array(
			array('name' => 'robots', 'content' => 'no cache'),
			array('name' => 'descripition', 'content' => 'SportMatch'),
			array('name' => 'keywords', 'content' => 'Esportes, Esportes Coletivos, jogo'),
			array('name' => 'robots', 'content' => 'no-cache'),
			array('name' => 'Content-type', 'content' => 'text/html;charset=utf-8', 'type' => 'equiv')
			);
		echo meta($meta);
		echo link_tag('bootstrap/css/bootstrap.css');
		echo link_tag('bootstrap/css/bootstrap-responsive.css');
		?>

		<style>
    /* GLOBAL STYLES
    -------------------------------------------------- */
    /* Padding below the footer and lighter body text */

        body {
        padding-bottom: 40px;
        color: #FFFFFF;
        background: url("../../bootstrap/img/bg_20.jpg") no-repeat;
        background-size: 100%;
    }


    .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        margin-top: 200px;
        background-color: #fff;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
        text-align: center;
      }
    </style>
	</head>
  <body background="<?php echo base_url().'bootstrap/img/bd.jpg'?>">
