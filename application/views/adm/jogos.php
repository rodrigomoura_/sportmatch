<div class="span9">
	<div class="hero-unit">
			<legend>Usuários Cadastrados</legend>
			<?php foreach($jogo as $u){ ?>
			<table class="table">
				<tr>
					<td><p><strong>Esporte:</strong><?php echo $u->tipo_jogo ?></p></td>
					<td><p><strong>Data:</strong><?php echo $u->data?></p></td>
					<td><p><strong>Horário de Início:</strong><?php echo $u->horario_inicio ?></p></td>
					<td><p><strong>Horário de Fim:</strong><?php echo $u->horario_fim ?></p></td>
				</tr>
				<tr>
					<td><p><strong>Participantes:</strong><?php echo $u->participantes ?></p></td>
					<td><p><strong>Endereço:</strong><?php echo $u->endereco ?></p></td>
					<td><p><strong>Cidade:</strong><?php echo $u->cidade ?></p></td>
					<td><p><strong>Estado:</strong><?php echo $u->estado ?></p></td>
				</tr>
				<tr><p alig="left"><a href="<?php echo base_url().'adm/gerencia/excluiJogo'.$u->id_jogo ?>" class="btn btn-danger">Excluir Jogo</a></p></tr>
				</table>
				<hr class="soften">
			<?php } ?>
		
	</div>
</div>