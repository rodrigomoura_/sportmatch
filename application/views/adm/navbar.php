<div class="container">
<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url().'adm/adm'; ?>"><img src="<?php echo base_url().'bootstrap/img/logo.png' ?>" heigh="50" width="130"></a>
          <div class="nav-collapse collapse">
            <ul class="nav pull-left">
              <li><?php echo anchor('adm/gerencia/usuarios','Usuários','title="Usuarios"'); ?></li>
              <li><?php echo anchor('adm/gerencia/jogos/','Jogos','title="Jogos"'); ?></li>
            </ul> 
            <ul class="nav pull-right">
              <li><a href="<?php echo base_url().'adm/gerencia/logout'?>"><strong>Sair!</strong></a></li>
            </ul>             
          </div>
        </div> 
      </div>
</div>
</div>

