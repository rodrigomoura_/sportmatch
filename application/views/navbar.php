<div class="container">
<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url().'inicio/'; ?>"><img src="<?php echo base_url().'bootstrap/img/logo.png' ?>" heigh="50" width="130"></a>
          <div class="nav-collapse collapse">
            <ul class="nav pull-left">
              <li><?php echo anchor('amigos/','Amigos','title="Amigos"'); ?></li>
              <li><?php echo anchor('jogos/','Jogos','title="Jogos"'); ?></li>
            </ul>
            <ul class="nav pull-right">
              <li id="fat-menu" class="dropdown">
                <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->session->userdata('usuario'); ?><b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                  <?php 
                    echo '<li>'.anchor(base_url().'home/conta','Minha Conta').'<li>';
                  ?>
                  <li class="divider"></li>
                    <?php 
                      echo '<li>'.anchor(base_url().'welcome/logout','Sair').'<li>';
                    ?>
                </ul>
              </li>
            </ul>              
          </div>
        </div> 
      </div>
</div>
</div>

