<div class="span9">
	<div class="hero-unit">
		<form name="amigo" action="<?php echo site_url('amigos/busca') ?>" method="POST"/>
			<table class="table table-condensed">
				<legend><h3>Procurar Amigos</h3></legend>
				<tr>
					<td><p align="center"><input type="text" name="nome" id="amigos" class="input-xxlarge search-query"></p></td>
					<td><p align="left"><input type="submit" class="btn btn-danger" value="Procurar"></p></td>
				</tr>
			</table>
		</form>
		<form name="disponibilidade" action="<?php echo site_url('disponibilidade/cadastra') ?>" method="POST"/>
			<table class="table table-condensed table-hover">
				<legend><h3>Procurar Jogos</h3></legend>
				<tr>
					<td><p align="right">Tipo Esporte:</p></td>
					<td>
						<select name="tipo_esporte">
							<option>Escolher Esporte</option>
							<?php foreach($jogos as $j){ ?>
								<option><?php echo $j->tipo; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><p align="right">Data:</p></td>
					<td><p align="left"><input type="text" name="data_jogo"></p></td>
				</tr>
				<tr>
					<td><p align="right">Hora de Início:</p></td>
					<td><p align="left"><input type="time" name="h_ini"></p></td>
					<td><p align="right">Hora de Fim:</p></td>
					<td><p align="left"><input type="time" name="h_fim"></p></td>
				</tr>
				<tr>
					<td><p align="right">Cidade:</p></td>
					<td>
						<select name="cidade">
							<?php foreach($cidades as $c){ ?>
								<option><?php echo $c->nome; ?></option>
							<?php } ?>
						</select>
					</td>
					<td><p align="right"><input type="submit" class="btn btn-danger" value="Buscar Jogo"></p></td>
				</tr>
			</table>
		</form>
	</div>
</div>
