<?php include("navbar.php");?>
<?php
foreach($user as $u):
$id = $u->id_face;
$nome = $u->name;
$email = $u->email;
endforeach;
?>

<div class="container" style="margin-top:50px;">
    <div class="row">
        <div class="offset4 span8">
            <h2 class="text-center login-title">Cadastro</h2>
            <div class="account-wall">
				<form class="" name="addUsuario" action="<?php echo site_url('home/addUsuario') ?>" method="POST">
					<div class="control-group">
						<label class="control-label">ID Facebook</label>
						<div class="controls">
							<input type="text" name="id_face" value="<?php echo $id; ?>" readonly="true">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nome</label>
						<div class="controls">
							<input type="text" name="nome" value="<?php echo $nome ?>" readonly="true">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Email</label>
						<div class="controls">
							<input type="text" name="email" value="<?php echo $email ?>" readonly="true">
						</div>
					</div>
						
					<div class="control-group">
						<label class="control-label">CPF</label>
						<div class="controls">
							<input type="text" name="cpf" placeholder="123.456.789-01">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Data de Nascimento</label>
						<div class="controls">
							<input type="date" name="dtNasc">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Sexo</label>
						<div class="controls">
							<select name="sexo">
								<option>Masculino</option>
								<option>Feminino</option>
							</select>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Endereço</label>
						<div class="controls">	
							<input type="text" name="rua" placeholder="Av. Roraima, 1000">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Bairro</label>
						<div class="controls">
							<input type="text" name="bairro" placeholder="Camobi">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Estado</label>
						<div class="controls">	
							<select name="estado" id="estado">
								<?php
								foreach($estados as $est){ ?>
									<option value="<?= $est->uf; ?>"><?= $est->nome ?></option>
								<?php //mudei aqui pra funcionar o script
								}
								?>
							</select>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Cidade</label>
						<div class="controls">
							<select name="cidade" id="cidade">
								<option value="">Selecione sua cidade</option>
							</select>
						</div>
					</div>
					<input class="btn btn-primary" type="submit" value="Finalizar Cadastro">
				</form>
			</div>
		</div>
	</div>
</div>
