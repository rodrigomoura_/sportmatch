<div class="span9">
	<div class="hero-unit">
		<center>
			<a href="<?php echo base_url().'jogos/novo_jogo'?>" class="btn btn-large btn-block btn-danger">Criar Novo Jogo</a>
		</center>
	</div>
	<div class="hero-unit">
		<?php if(count($jogos)==0){ ?>
			<center><h3><strong>Oops! Nenhum Jogo Encontrado.</strong></h3></center>

	<?php	} ?>
		<?php foreach($jogos as $j){
			if($j->id_criador == $this->session->userdata('id')){ ?>
				<table class="table">
					<legend><h3>Meus Jogos</h3></legend>
					<tr>
						<td><p align="right"><strong>Esporte:</strong></p></td>
						<td><p align="left"><?php echo $j->tipo_jogo; ?></p></td>
						<td>|</td>
						<td><p align="right"><strong>Horário:</strong></p></td>
						<td><p align="left"><?php echo $j->horario_inicio; ?></p></td>
						<td>|</td>
						<td><p align="right"><strong>Local:</strong></p></td>
						<td><p align="left"><?php echo $j->cidade.'; '.$j->endereco; ?></p></td>
					</tr>
				</table>
				<center>
					<table>
						<tr>
							<td><a href="<?php echo base_url().'jogos/maisDetalhes/'.$j->id_jogo; ?>" class="btn"><i class="icon-plus"></i> Mais Detalhes</a></td>
							<td><a href="<?php echo base_url().'jogos/editaJogo/'.$j->id_jogo; ?>" class="btn btn-warning"><i class="icon-edit"></i> Editar Jogo</a></td>
							<td><a href="<?php echo base_url().'jogos/excluiJogo/'.$j->id_jogo; ?>" class="btn btn-danger"><i class="icon-remove"></i> Excluir Jogo</a></td>
						</tr>
					</table>
					<hr class="soften">
				</center>
			<?php } ?>
		<?php } ?>
		<?php foreach($jogos as $j){ 
			if($j->id_criador != $this->session->userdata('id')){ ?>
				<table class="table">
					<legend><h3>Outros Jogos</h3></legend>
					<?php foreach($usuario as $u){
						if($u->id_facebook == $j->id_criador){ ?>
							<tr><p>Convidado por:<?php echo $u->nome; ?></p></tr>
					<?php }
						} ?>
					<tr>
						<td><p align="right"><strong>Esporte:</strong></p></td>
						<td><p align="left"><?php echo $j->tipo_jogo; ?></p></td>
						<td>|</td>
						<td><p align="right"><strong>Horário:</strong></p></td>
						<td><p align="left"><?php echo $j->horario_inicio; ?></p></td>
						<td>|</td>
						<td><p align="right"><strong>Local:</strong></p></td>
						<td><p align="left"><?php echo $j->cidade.'; '.$j->endereco; ?></p></td>
					</tr>
				</table>
				<center>
					<table>
						<tr>
							<td><a href="<?php echo base_url().'jogos/maisDetalhes/'.$j->id_jogo; ?>" class="btn"><i class="icon-plus"></i> Mais Detalhes</a></td>
						</tr>
					</table>
					<hr class="soften">
				</center>
				<?php } ?>
			<?php } ?>
	</div>
</div>