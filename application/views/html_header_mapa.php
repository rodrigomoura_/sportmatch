<?php echo doctype('xhtml1-trans'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SportMatch</title>
		<?php
		$meta = array(
			array('name' => 'robots', 'content' => 'no cache'),
			array('name' => 'descripition', 'content' => 'SportMatch'),
			array('name' => 'keywords', 'content' => 'Esportes, Esportes Coletivos, jogo'),
			array('name' => 'robots', 'content' => 'no-cache'),
			array('name' => 'Content-type', 'content' => 'text/html;charset=utf-8', 'type' => 'equiv')
			);
		echo meta($meta);
		echo link_tag('bootstrap/css/bootstrap.css');
		echo link_tag('bootstrap/css/bootstrap-responsive.css');
    echo link_tag('bootstrap/jquery/jquery-ui.css');
		?>

		<style>
    /* GLOBAL STYLES
    -------------------------------------------------- */
    /* Padding below the footer and lighter body text */

    body {
      background-color: #FFFFFF;
      padding-bottom: 40px;
      color: #040404;
    }

    .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        margin-top: 200px;
        background-color: #fff;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
        text-align: center;
      }
      .container-fluid{
        margin-top: 70px;
      }
    </style>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#estado").change(function() {
                $("#estado option:selected").each(function() {
                    estado = $('#estado').val();
                    $.post("http://localhost/php/SportMatch/home/pega_cidades", {
                        estado : estado
                    }, function(data) {
                        $("#cidade").html(data);
                    });
                });
            })
        });
    </script>
    <script type="text/javascript">
      $(function(){
        $("#amigos").autocomplete({
          source: "amigos/get_amigos" 
        });
      });
    </script>
	</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=1488917458037056&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>