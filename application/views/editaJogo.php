<div class="span9">
	<div class="hero-unit">
		<h2 class="text-center login-title">Novo Jogo</h2>
		<form class="" name="addJogo" action="<?php echo base_url().'jogos/finalizaEdicao' ?>" method="POST">
			<div class="control-group">
				<label class="control-label">Criado Por:</label>
				<div class="controls">
					<input type="text" name="criador" value="<?php echo $this->session->userdata('usuario'); ?>" readonly="true">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Esporte</label>
				<div class="controls">
					<select name="tipoEsporte">
						<option><?php echo $jogo[0]->tipo_jogo ?></option>
						<?php foreach($esportes as $e){ ?>
							<option><?php echo $e->tipo ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Data:</label>
				<div class="controls">
					<input type="text" name="data" value="<?php echo $jogo[0]->data ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Horário de Início:</label>
				<div class="controls">
					<input type="text" name="horario_inicio" value="<?php echo $jogo[0]->horario_inicio ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Horário de Fim:</label>
				<div class="controls">
					<input type="text" name="horario_fim" value="<?php echo $jogo[0]->horario_fim ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Número de Participantes:</label>
				<div class="controls">
					<input type="text" name="participantes" value="<?php echo $jogo[0]->participantes ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Cidade:</label> <!--arrumar select -->
				<div class="controls">
					<select name="cidade">
						<option><?php echo $jogo[0]->cidade ?></option>
						<?php foreach($cidades as $c){ ?>
							<option><?php echo $c->nome; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Bairro:</label>
				<div class="controls">
					<input type="text" name="bairro" value="<?php echo $jogo[0]->bairro ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Endereço:</label>
				<div class="controls">
					<input type="text" name="endereco" value="<?php echo $jogo[0]->endereco ?>">
				</div>
			</div>
			<input class="btn btn-danger" type="submit" value="Finalizar Edição">
		</form>
	</div>
</div>