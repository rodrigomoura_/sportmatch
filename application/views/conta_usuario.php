<div class="container" style="margin-top:50px;">
    <div class="row">
        <div class="offset4 span8">
            <h2 class="text-center login-title">Cadastro</h2>
            <div class="account-wall">
				<form class="" name="addUsuario" action="<?php echo site_url('home/editaUsuario') ?>" method="POST">
					<div class="control-group">
						<label class="control-label">ID Facebook</label>
						<div class="controls">
							<input type="text" name="id_face" value="<?php echo $usuario[0]->id_facebook; ?>" readonly="true">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nome</label>
						<div class="controls">
							<input type="text" name="nome" value="<?php echo $usuario[0]->nome ?>" readonly="true">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Email</label>
						<div class="controls">
							<input type="text" name="email" value="<?php echo $usuario[0]->email; ?>">
						</div>
					</div>
						
					<div class="control-group">
						<label class="control-label">CPF</label>
						<div class="controls">
							<input type="text" name="cpf" value="<?php echo $usuario[0]->cpf; ?>">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Data de Nascimento</label>
						<div class="controls">
							<input type="date" name="dtNasc" value="<?php echo $usuario[0]->data_nascimento; ?>">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Sexo</label>
						<div class="controls">
							<select name="sexo">
								<option>Masculino</option>
								<option>Feminino</option>
							</select>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Endereço</label>
						<div class="controls">	
							<input type="text" name="rua" value="<?php echo $usuario[0]->rua; ?>">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Bairro</label>
						<div class="controls">
							<input type="text" name="bairro" value="<?php echo $usuario[0]->bairro; ?>">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Estado</label>
						<div class="controls">	
							<select name="estado" id="estado">
								<?php
								foreach($estados as $est){ ?>
									<option value="<?= $est->uf; ?>"><?= $est->nome ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Cidade</label>
						<div class="controls">
							<select name="cidade" id="cidade">
								<option value="">Selecione sua cidade</option>
							</select>
						</div>
					</div>
					<input class="btn btn-primary" type="submit" value="Atualizar Cadastro">
				</form>
			</div>
		</div>
	</div>
</div>
