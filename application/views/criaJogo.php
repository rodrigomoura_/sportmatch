<div class="span9">
	<div class="hero-unit">
		<h2 class="text-center login-title">Novo Jogo</h2>
		<form class="" name="addJogo" action="<?php echo base_url().'jogos/novoJogo' ?>" method="POST">
			<div class="control-group">
				<label class="control-label">Criado Por:</label>
				<div class="controls">
					<input type="text" name="criador" value="<?php echo $this->session->userdata('usuario'); ?>" readonly="true">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Esporte</label>
				<div class="controls">
					<select name="tipoEsporte">
						<option>Selecione o Esporte</option>
						<?php foreach($esportes as $e){ ?>
							<option><?php echo $e->tipo ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Data:</label>
				<div class="controls">
					<input type="date" name="data" placeholder="01/01/2014">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Horário de Início:</label>
				<div class="controls">
					<input type="time" name="horario_inicio">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Horário de Fim:</label>
				<div class="controls">
					<input type="time" name="horario_fim">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Número de Participantes:</label>
				<div class="controls">
					<input type="text" name="participantes">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Cidade:</label> <!--arrumar select -->
				<div class="controls">
					<select name="cidade">
						<?php foreach($cidades as $c){ ?>
							<option><?php echo $c->nome; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Bairro:</label>
				<div class="controls">
					<input type="text" name="bairro">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Endereço:</label>
				<div class="controls">
					<input type="text" name="endereco">
				</div>
			</div>
			<input class="btn btn-danger" type="submit" value="Criar Jogo">
		</form>
	</div>
</div>