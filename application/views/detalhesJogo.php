<div class="span9">
	<div class="hero-unit">
		<center>
			<table class="table table-bordered">
				<legend><h3>Detalhes - Jogo <?php echo $jogo[0]->data;?></h3></legend>
				<?php
				if($jogo[0]->id_criador == $this->session->userdata('id')){ ?>
					<tr>
						<td><p align="left"><a href="<?php echo base_url().'jogos/editaJogo/'.$jogo[0]->id_jogo; ?>" class="btn btn-warning"><i class="icon-edit"></i> Editar Jogo</a></p></td>
						<td><p align="left"><a href="<?php echo base_url().'jogos/enviaNota/'.$jogo[0]->id_jogo; ?>" class="btn btn-inverse"><i class="icon-pencil"></i>Dar Notas</a></p></td>
					</tr>
				<?php } ?>
				<tr>
					<td><p align="left"><strong>Esporte:</strong> <?php echo $jogo[0]->tipo_jogo; ?></p></td>
					<td><p align="left"><strong>Participantes:</strong> <?php echo $jogo[0]->participantes; ?></p></td>
				</tr>
				<tr>
					<td><p align="left"><strong>Horario de Início:</strong> <?php echo $jogo[0]->horario_inicio; ?></p></td>
					<td><p align="left"><strong>Horário de Fim:</strong> <?php echo $jogo[0]->horario_fim; ?></p></td>
				</tr>
				<tr>
					<td><p align="left"><strong>Cidade:</strong> <?php echo $jogo[0]->cidade; ?></p></td>
					<td><p align="left"><strong>Endereço:</strong> <?php echo $jogo[0]->endereco; ?></p></td>
				</tr>
				<tr>
					<td><p align="left"><a href="<?php echo base_url().'jogos/verMapa/'.$jogo[0]->id_jogo; ?>" class="btn btn-danger"><i class="icon-map-marker"></i>Ver Mapa</a></p></td>
				</tr>
			</table>

			<table class="table table-striped table-condensed">
				<legend>Jogadores Confirmados - <?php echo ' Vagas Disponíveis: '.(((int)($jogo[0]->participantes))-count($jogoUsuario)); ?></legend>
				<?php for($i=0;$i<count($jogoUsuario);$i+=2){ ?>
					<tr>
						<td><p><?php echo $i+1 .' '; ?></p></td>
						<td><p align="left"><?php echo $jogoUsuario[$i]->nome; ?></p></td>
						<?php if(count($jogoUsuario)>1){ ?>
						<td><p><?php echo $i+2 .' '; ?></p></td>
						<td><p align="left"><?php echo $jogoUsuario[$i+1]->nome; ?></p></td>
						<?php } ?>
					</tr>
				<?php } ?>
				
			</table>
			<table>
				<?php if(count($jogoUsuario) < ((int)$jogo[0]->participantes)){
						if(count($confirmado)==0){?>
						<tr>
							<p align="center"><a href="<?php echo base_url().'jogos/participar/'.$jogo[0]->id_jogo; ?>" class="btn btn-danger"><i class="icon-plus"></i>Participar</a></p>
						</tr>
						<?php }else{ ?>
							<tr>
								<p align="center"><a href="<?php echo base_url().'jogos/participar/'.$jogo[0]->id_jogo; ?>" class="btn btn-danger" disabled><i class="icon-plus"></i>Participar</a></p>
							</tr>
							<?php } ?>
					<?php } ?>
			</table>
		</center>
		<div class="fb-comments" data-href="http://localhost/php/SportMatch/jogos/maisDetalhes/<?php echo $jogo[0]->id_jogo?>" data-numposts="20" data-colorscheme="light"></div>
	</div>
</div>