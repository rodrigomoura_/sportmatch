<?php
class Usuario extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}

	public function verificaID($id){
		$query = $this->db->get_where('usuario',array('id_facebook' => $id))->result();
		return $query;
	}

	public function addUsuario_model($dados){
		return $this->db->insert('usuario', $dados);
	}

	public function excluiUsuario_model($id){
		$this->db->where('id',$id);
		return $this->db->delete('usuario');
	}

	public function excluiDadoProvisorio_model($id){
		$this->db->where('id_face',$id);
		$this->db->delete('dadosprovisorios');
	}

	public function pegaNome($id){
		$this->db->where('id_face', $id);
		return $this->db->get('dadosprovisorios')->result();
	}

	function get_usuarios(){
		$this->db->select('nome,id_facebook');
		$query = $this->db->get('usuario')->result();

		return $query;
	}

	function pegaIDreal($idUser){
		$this->db->where('id_facebook', $idUser);
		$query = $this->db->get('usuario')->result();

		return $query;
	}

	function pega_usuario($idUser){
		$this->db->where('id_facebook', $idUser);
		$query = $this->db->get('usuario')->result();

		return $query;
	}

	public function editaUsuario_model($dados, $idUser){
		$this->db->where('id_facebook', $idUser);
		return $this->db->update('usuario', $dados);
	}
}