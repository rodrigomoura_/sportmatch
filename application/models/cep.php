<?php
class Cep extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}

	function estados(){
		$estados = $this->db->get('tb_estados')->result();
		return $estados;
	}

	function cidade($estado){
		//$this->db->where('estado',$estado);
		$cidades = $this->db->get_where('tb_cidades', array('uf'=> $estado));
		return $cidades->result();
	}

	function pega_estado($id){
		$this->db->where('id_facebook', $id);
		$query = $this->db->get('usuario')->result();

		return $query;
	}

	function pega_cidade($uf){
		$this->db->where('uf', $uf);
		$query = $this->db->get('tb_cidades')->result();

		return $query;
	}

	/*function pega_cidade($id){
		//$this->db->select('estado');
		//$query = $this->db->get_where('usuario', array('id_facebook' => $id));
		$estado = 'RS';
		$query = $this->db->get_where('tb_cidades', array('uf' => $estado));

		return $query->result();
	}*/

	function endereco_user($idUser){
		$this->db->where('id_facebook', $idUser);
		$query = $this->db->get('usuario')->result();

		return $query;
	}

	function endereco_jogo($idJogo){
		$this->db->where('id_jogo', $idJogo);
		$query = $this->db->get('jogo')->result();

		return $query;
	}
}