<?php
class Jogo_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}

	function pega_jogo(){
		$this->db->order_by('tipo');
		$query = $this->db->get('tipo_esportes');

		return $query->result();
	}

	function busca_jogos($id_user){
		$query = $this->db->get_where('jogo_usuario', array('id_usuario' => $id_user))->result();

		return $query;
	}

	function busca_jogos2($id_user){
		$query = $this->db->query("select * from jogo j
									inner join jogo_usuario u on j.id_jogo = u.id_jogo and u.id_usuario ="
									.$id_user)->result();

		return $query;
	}

	function get_jogo($idJogo){
		$this->db->where('id_jogo', $idJogo);
		$query = $this->db->get('jogo')->result();

		return $query;
	}

	function get_jogo_usuario($idJogo){
		/*$this->db->where('id_jogo',$idJogo);
		$this->db->get('jogo_usuario');*/
		$query = $this->db->query("select * from usuario u
									inner join jogo_usuario j on j.id_usuario = u.id_facebook and j.tipo = 'confirmado' and j.id_jogo =".$idJogo)->result();

		return $query;
	}

	function get_usuario_confirmado($id_user, $idJogo){
		$str = 'confirmado';
		$query = $this->db->get_where('jogo_usuario', array('id_usuario' => $id_user, 'id_jogo' => $idJogo, 'tipo' => $str))->result();

		return $query;
	}

	function novo_jogo($data){
		return $this->db->insert('jogo', $data);
	}

	function pegaJogo($id){
		$query = $this->db->query("select * from jogo where id_criador =".$id)->result();

		return $query;
	}

	function insereJogoCriador($dados2){
		return $this->db->insert('jogo_usuario',$dados2);
	}

	function convida_amigos($dados){
		return $this->db->insert('jogo_usuario', $dados);
	}

	function exclui_jogo($idJogo){
		$tables = array('jogo_usuario','jogo');
		$this->db->where('id_jogo', $idJogo);
		return $this->db->delete($tables);
	}

	function exclui_jogo_usuario($id){
		$this->db->where('id_jogo', $idJogo);
		return $this->db->delete('jogo_usuario');	
	}

	function edita_jogo($data, $id){
		$this->db->where('id_jogo', $id);
		return $this->db->update('jogo');
	}

	function confirma_participacao($data, $idUser, $idJogo){
		$this->db->where('id_jogo',$idJogo);
		$this->db->where('id_usuario', $idUser);
		return $this->db->update('jogo_usuario', $data);
	}

	function nota($data, $idUser){
		return $this->db->insert('notas',$data);
	}

	function pegaNnotas($idUsuario){
		$this->db->where('id_usuario_jogo', $idUsuario);
		$query = $this->db->get('notas')->result();

		return $query;
	}

	function update_media($idUser, $mediaNota){
		$data['media_nota'] = (int)$mediaNota;
		$this->db->where('id_facebook', $idUser);
		$this->db->update('usuario', $data);
	}
}