<?php
class Amigos_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}

	function get_amigo($busca){
   		$this->db->select('nome');    
    	$this->db->like('nome', $busca);
    	$query = $this->db->get('usuario');
    	if($query->num_rows > 0){
      		foreach ($query->result_array() as $row){
        		$row_set[] = htmlentities(stripslashes($row['nome'])); //build an array
      		}
      		echo json_encode($row_set); //format the array into json data
    	}
  	}  

    function like_amigos($nome){
      return $query = $this->db->query('SELECT * FROM usuario WHERE nome LIKE "%'.$nome.'%"')->result();
    }

    function pegaIDAmigo($nome){
      $this->db->select('id_facebook');
      $id_amigo = $this->db->get_where('usuario',array('nome' => $nome))->result();

      return $id_amigo;
    }

    function pega_amizade($id_amizade,$id_user){
      $amizade = $this->db->get_where('amizade', array('id_usuario1' => $id_user, 'id_usuario2' => $id_amizade))->result();

      return $amizade;
    }

    function nova_amizade($id_user,$idNovoAmigo){
      $data['id_usuario1'] = $id_user;
      $data['id_usuario2'] = $idNovoAmigo;
      $data['tipo'] = 'pendente';
      $this->db->insert('amizade',$data);

      $dado['id_usuario1'] = $idNovoAmigo;
      $dado['id_usuario2'] = $id_user;
      $dado['tipo'] = 'amizade';
      $this->db->insert('amizade',$dado);
    }

    function get_amigos_amizade($id_user){
      $query = $this->db->query("select * from amizade o
                                inner join usuario u ON o.id_usuario1 = ".$id_user."
                                group by u.nome")->result();
      return $query;
    }

    function exclui_amigo($id){
      $this->db->where('id_usuario1',$id);
      $this->db->delete('amizade');
      $this->db->where('id_usuario2',$id);
      $this->db->delete('amizade');
    }

    function get_lista_amigos($id){
      $query = $this->db->get_where('amizade', array('id_usuario2' => $id, 'tipo' => 'amizade'))->result();

      return $query;
    }
}